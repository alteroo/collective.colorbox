Documentation
=============
Very useful for creating an image gallery
This product loads the colorbox plugin for Plone.

to see it in action after installation visit the @@colorbox.demo page

Based on http://colorpowered.com/colorbox/

Changelog
=========

0.1a (2010-07-16)
------------------

* Initial (alpha) release [pigeonflight]

